﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class ButtonManager : MonoBehaviour
{
    public List<RectTransform> buttons;
    bool start = false;
    float counter;

    int animationStatus = 0;
    float animationDuration, animationProgress, animationElapsedTime;
    // Start is called before the first frame update
    void Start()
    {
        counter = 0.0f;
        start = true;
    }

    void FadeBlack()
    {
        animationStatus = 1;
        animationDuration = 1;
        animationProgress = 0.0f;
        animationElapsedTime = 0.0f;
    }

    void MoveHexagons()
    {
        animationStatus = 2;
        animationDuration = 1.5f;
        animationProgress = 0.0f;
        animationElapsedTime = 0.0f;
    }

    // Update is called once per frame
    void FixedUpdate()
    {
        if(start)
        {
            counter+= Time.deltaTime;
            if(counter >= 1)
            {
                start = false;
                FadeBlack();
            }
        }
        if (animationStatus != 0)
        {
            animationElapsedTime += Time.deltaTime;
            animationProgress = animationElapsedTime / animationDuration;
            if (animationProgress >= 1)
            {
                if(animationStatus == 1)
                {
                    buttons[4].gameObject.SetActive(false);
                    MoveHexagons();
                }
                else if( animationStatus == 2)
                {
                    buttons[0].anchoredPosition = new Vector3(0, buttons[0].anchoredPosition.y, 0);
                    buttons[1].anchoredPosition = new Vector3(91, buttons[1].anchoredPosition.y, 0);
                    buttons[2].anchoredPosition = new Vector3(-91, buttons[2].anchoredPosition.y, 0);
                    buttons[3].anchoredPosition = new Vector3(0, buttons[3].anchoredPosition.y, 0);
                    animationStatus = 0;
                    GridManager.instance.gameState = 1;
                }
            }
            else
            {
                if (animationStatus == 1)
                {
                    buttons[4].transform.GetComponent<Image>().color = new Color(buttons[4].transform.GetComponent<Image>().color.r, buttons[4].transform.GetComponent<Image>().color.g, buttons[4].transform.GetComponent<Image>().color.b, 1 - animationProgress);
                }
                else if (animationStatus == 2)
                {
                    buttons[0].anchoredPosition = new Vector3(LinearMovement(692, 0, animationProgress), buttons[0].anchoredPosition.y, 0);
                    buttons[1].anchoredPosition = new Vector3(LinearMovement(783, 91, animationProgress), buttons[1].anchoredPosition.y, 0);
                    buttons[2].anchoredPosition = new Vector3(LinearMovement(-783, -91, animationProgress), buttons[2].anchoredPosition.y, 0);
                    buttons[3].anchoredPosition = new Vector3(LinearMovement(-692, 0, animationProgress), buttons[3].anchoredPosition.y, 0);
                }
            }
        }
    }

    float LinearMovement(float startPos, float EndPos, float _animationProgress)
    {
        return startPos + (EndPos - startPos) * _animationProgress;
    }

    float ProgressFromInterval(float start, float end, float _animationProgress)
    {
        if(_animationProgress >= start)
        {
            if(_animationProgress <= end)
            {
                return (_animationProgress - start) * (1 / (end - start));
            }
            return 1;
        }
        return 0;
    }
}

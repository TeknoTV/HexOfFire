﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

#region GridType
public enum GridType
{
    Hexagon,
    Lines
}
#endregion

public class GridManager : MonoBehaviour
{
    #region Singleton
    public static GridManager instance;

    public List<List<Hexagon>> grid;
    public int gameState = 0;

    public float hexWidth = 1.732f;
    public float hexHeight = 2f;
    public float gap = 0f;

    private void Awake()
    {
        if (instance == null)
            instance = this;

        else if (instance != this)
            Destroy(gameObject);

        //DontDestroyOnLoad(gameObject);
    }
    #endregion

    [SerializeField]
    int mapLength = 20;

    [SerializeField]
    Transform hexGeneratorPrefab;
    [SerializeField]
    GridType gridType = GridType.Lines;
    [SerializeField]
    int width = 3, height = 3;
    [SerializeField]
    float speed = 1f;
    [SerializeField]
    float speedScale = 1.05f;

    // ### Movement auxiliars
    int auxCounter;
    List<Hexagon> auxList;

    // ### UI
    public Canvas UICanvas, menuCanvas;
    public Text countdown;

    // ### Time 
    float timeCounter;

    void Start()
    {
        AddGap();
        grid = new List<List<Hexagon>>();
        auxList = new List<Hexagon>();
        for (int i = 0; i < mapLength; i++)
        {
            float offset = 0f;
            if (i % 2 != 0)
            {
                offset = 0.5f;
            }
            Transform obj = Instantiate(hexGeneratorPrefab, new Vector3(offset, 0, i * 0.75f), Quaternion.identity, this.transform);
            obj.GetComponent<HexGenerator>().SetType(gridType, width, height);
        }
    }
    private void AddGap()
    {
        hexWidth += hexWidth * gap;
        hexHeight += hexHeight * gap;
    }

    private void Update()
    {
        if (gameState == 1)
        {
            countdown.gameObject.SetActive(true);
            countdown.text = "" + (3 - (int)timeCounter);
            timeCounter += Time.deltaTime;
            if (timeCounter >= 3)
            {
                countdown.gameObject.SetActive(false);
                gameState = 100;
                timeCounter = 0;
            }
            
        }
        else if (gameState == 100)
        {
            timeCounter += Time.deltaTime;
            if(timeCounter > 10)
            {
                speed *= speedScale;
                timeCounter = 0;
            }
            foreach (List<Hexagon> l in grid)
            {
                foreach (Hexagon h in l)
                {
                    h.Pos -= new Vector3(0f, 0f, Time.deltaTime * speed);
                }
            }
            if (grid[0][0].Pos.z < -hexHeight * 0.75f)
            {
                auxCounter = 0;
                auxList = grid[0];
                grid.Remove(grid[0]);
                foreach (Hexagon hexagon in auxList)
                {
                    if(hexagon.Type/10 == 2)
                    {
                        gameState = 1000;
                        break;
                    }
                    hexagon.Pos = new Vector3(hexagon.Pos.x, 0, grid[mapLength - 2][auxCounter].Pos.z + hexHeight * 0.8f );
                    hexagon.DrawObject(hexagon.RandomizeType());
                    auxCounter++;
                }
                grid.Insert(mapLength - 1, auxList);
                UICanvas.GetComponent<PlayerController>().UpdateInfo();
            }
    }
        else if(gameState == 1000)
        {
            menuCanvas.gameObject.SetActive(true);
        }
    }
}

﻿//using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HexGenerator : MonoBehaviour
{
    public Transform hexPlaceholderPrefab;

    public int gridRadius;

    public int gridWidth;
    public int gridHeight;

    GridType HexGenerationType;

    float hexWidth, hexHeight, gap;

    List<Vector2> hexagonsToDraw;
    public List<Hexagon> hexagonsDrawn;

    private void Start()
    {
        hexWidth = GridManager.instance.hexWidth;
        hexHeight = GridManager.instance.hexHeight;
        gap = GridManager.instance.gap;
        hexagonsToDraw = new List<Vector2>();
        hexagonsDrawn = new List<Hexagon>();

        if (HexGenerationType == GridType.Hexagon)
        {
            AddGap();
            CreateHexagonGrid();
            DrawHexagons();
        }
        else if (HexGenerationType == GridType.Lines)
        {
            AddGap();
            CreateGrid();
            DrawHexagons();
        }
    }
    private void Update()
    {
        if (HexGenerationType == GridType.Hexagon)
        {
            if (Input.GetButtonDown("Fire1"))
            {
                ExpandHexagonGrid(gridRadius + 1);
            }
        }
    }

    #region Common
    private void AddGap()
    {
        hexWidth += hexWidth * gap;
        hexHeight += hexHeight * gap;
    }

    private Vector3 CalcWorldPos(Vector2 gridPos)
    {
        float offset = 0;
        if (gridPos.y % 2 != 0)
            offset = hexWidth / 2;

        float x = transform.position.x * hexWidth + gridPos.x * hexWidth + offset;
        float z = transform.position.z * hexHeight + gridPos.y * hexHeight * 0.75f;

        return new Vector3(x, 0, z);
    }
    private void DrawHexagons()
    {
        int count = hexagonsToDraw.Count;
        int randomValue;
        Vector3 worldPos;
        for (int i = 0; i < count; i++)
        {
            worldPos = CalcWorldPos(hexagonsToDraw[0]);
            Transform hex = Instantiate(hexPlaceholderPrefab, worldPos, Quaternion.identity, this.transform);
            hex.GetComponent<Hexagon>().Pos = worldPos;
            hex.name = "Hexagon" + hexagonsToDraw[0].x + "|" + hexagonsToDraw[0].y;
            randomValue = Random.Range(0, 3);
            if (i == count / 2 + 1 && transform.position.z == 3 * 0.75)
            {
                hex.GetComponent<Hexagon>().DrawObject(20 + randomValue);
            }
            else if ((i == count / 2 || i == count / 2 + 2) && transform.position.z == 3 * 0.75)
            {
                hex.GetComponent<Hexagon>().DrawObject(10 + randomValue);
            }
            else
            {
                hex.GetComponent<Hexagon>().DrawObject(hex.GetComponent<Hexagon>().RandomizeType());
            }
            hexagonsDrawn.Add(hex.GetComponent<Hexagon>());
            hexagonsToDraw.Remove(hexagonsToDraw[0]);
        }
        GridManager.instance.grid.Add(hexagonsDrawn);
    }
    #endregion

    #region GridType.Hexagon
    private void CreateHexagonGrid()
    {
        int xCount0 = 0, xCount1 = 0;

        for (int y = -gridRadius; y <= gridRadius; y++)
        {
            if (y == 0)
            {
                xCount0 = 0;
                xCount1 = 1;
            }
            else if (y < 0)
            {
                if (y == -gridRadius)
                {
                    if (gridRadius % 2 == 0)
                    {
                        xCount0 = gridRadius / 2;
                        xCount1 = gridRadius / 2;
                    }
                    else
                    {
                        xCount0 = gridRadius / 2;
                        xCount1 = gridRadius / 2 + 1;
                    }
                }
                else if (y % 2 == 0)
                {
                    xCount1--;
                }
                else
                {
                    xCount0--;
                }
            }
            else if (y > 1)
            {
                if (y % 2 == 0)
                {
                    xCount0++;
                }
                else
                {
                    xCount1++;
                }
            }

            for (int x = -gridRadius; x <= gridRadius; x++)
            {
                if (y == 0 || x >= -gridRadius + xCount0 && x <= gridRadius - xCount1)
                {
                    hexagonsToDraw.Add(new Vector2(x, y));
                }
            }
        }
    }

    private void ExpandHexagonGrid(int _radius)
    {
        gridRadius = _radius;

        CreateHexagonGrid();
        CompareToDrawAndDrawn();
        DrawHexagons();
    }

    private void CompareToDrawAndDrawn()
    {
        int drawnCount = hexagonsDrawn.Count;
        int toDrawCount = hexagonsToDraw.Count;

        for (int i = 0; i < drawnCount; i++)
        {
            for (int j = 0; j < toDrawCount; j++)
            {
                if (hexagonsDrawn[i].Pos.x == hexagonsToDraw[j].x && hexagonsDrawn[i].Pos.z == hexagonsToDraw[j].y)
                {
                    hexagonsToDraw.Remove(hexagonsToDraw[j]);
                    toDrawCount--;
                }
            }
        }
    }

    #endregion

    #region GridType.Lines
    private void CreateGrid()
    {
        for (int y = -gridHeight; y <= gridHeight; y++)
        {
            for (int x = -gridWidth; x <= gridWidth; x++)
            {
                hexagonsToDraw.Add(new Vector2(x, y));
            }
        }
    }
    #endregion

    public void SetType(GridType type, int width, int height)
    {
        HexGenerationType = type;
        if (type == GridType.Hexagon)
        {
            gridRadius = width;
        }
        else if (type == GridType.Lines)
        {
            gridWidth = width;
            gridHeight = height;
        }
    }
}
